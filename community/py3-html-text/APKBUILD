# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-html-text
#_pkgreal is used by apkbuild-pypi to find modules at PyPI
_pkgreal=html_text
pkgver=0.6.0
pkgrel=1
pkgdesc="Extract text from HTML"
url="https://pypi.python.org/project/html-rext"
arch="noarch"
license="MIT"
depends="py3-lxml"
checkdepends="py3-pytest py3-parsel py3-six"
makedepends="py3-setuptools py3-gpep517 py3-wheel"
source="$pkgname-$pkgver.tar.gz::https://pypi.io/packages/source/h/html_text/html_text-$pkgver.tar.gz"
builddir="$srcdir/$_pkgreal-$pkgver"
subpackages="$pkgname-pyc"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer "$builddir"/.dist/*.whl
	.testenv/bin/python3 -m pytest -v
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}
sha512sums="
2d9d3002d3dabcc47eedef5502e418aac59cdf1cc187dbaa3b07b3830a78fb8e0573f6d8422acd0834b8083c3d3ed3973fd53011ea41082d5ba6ac53b822d01c  py3-html-text-0.6.0.tar.gz
"
