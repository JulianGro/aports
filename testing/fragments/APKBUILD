# Contributor: NekoCWD <nekodevelopper@gmail.com>
# Maintainer: NekoCWD <nekodevelopper@gmail.com>
pkgname=fragments
pkgver=2.1.1
pkgrel=0
pkgdesc="A BitTorrent Client"
url="https://gitlab.gnome.org/World/Fragments"
# s390x fails to build
# riscv64: rust broken
arch="all !s390x !riscv64"
license="GPL-3.0-or-later"
depends="transmission-daemon"
makedepends="
	cargo
	dbus-dev
	desktop-file-utils
	libadwaita-dev
	openssl-dev
	m4
	meson
	"
subpackages="$pkgname-lang"
source="
	https://gitlab.gnome.org/World/Fragments/-/archive/$pkgver/Fragments-$pkgver.tar.gz
	no-cargo-home.patch
	"
options="net" # needed for downloading dependencies
builddir="$srcdir/Fragments-$pkgver" # Name starts with capital letter

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	# without buildtype=release it builds debug
	abuild-meson \
		--buildtype=release \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
c4455f7f12a9abaaf42feb74ff07f49d5a91b0d50d1d80cc7980b8e6147146dd8784e766a36d5503ade6c663c3d42fc19573ff9a54da34f614549bc1ad91a003  Fragments-2.1.1.tar.gz
2c4b7433f27b18bbb428bdc2053cda53f10e958a2045499ab7af6102df9610822cf36a82636cd5b9501ba4e204aeddc18fcdb79aff773657a9a5a3c1aeba8e95  no-cargo-home.patch
"
